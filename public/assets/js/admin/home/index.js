$(document).ready(function() {

  // Excluir Imagem
  $(".delete-file").on('click', function(e) {
    var obj = $(this);
    var tr = obj.parents('tr').eq(0);
    var href = obj.attr('href');
    if (href) {
      alertify.set({labels: {
          ok: "Ok",
          cancel: "Cancelar"
        }});
      alertify.confirm("Deseja realmente excluir essa imagem?", function(e) {
        if (e) {
          $.ajax({
            dataType: "json",
            type: "POST",
            url: href
          }).done(function(response) {
            alertify.alert(response.msg, function(e) {
              if (e && response.status) {
                tr.remove();
              }
            });
          });
        }
      });
    }
  });

});
