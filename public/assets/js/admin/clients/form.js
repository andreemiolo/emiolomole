$(document).ready(function() {
  var options = {
    success: showResponse,
    error: function(e) {
    },
    type: 'post',
    dataType: 'json'
  };
  $('.form-upload').submit(function() {
    $(this).ajaxSubmit(options);
    return false;
  });
});

function showResponse(responseText, statusText, xhr, $form) {
  alertify.alert(responseText.msg, function(e) {
    if (e && responseText.status) {
      location.reload();
    }
  });
}
