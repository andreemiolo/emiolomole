$(document).ready(function() {

  $("#image").on('change', function() {
    var files = !!this.files ? this.files : [];
    if (!files.length || !window.FileReader)
      return; // no file selected, or no FileReader support

    if (/^image/.test(files[0].type)) { // only image file
      var reader = new FileReader(); // instance of the FileReader
      reader.readAsDataURL(files[0]); // read the local file
      reader.onloadend = function() { // set image data as background of div
        var result = this.result;
        var img = new Image();
        img.src = this.result;
        img.onload = function() {
          var width = this.width;
          var height = this.height;
          $("#preview-image").html("<img width='450' src='" + result + "' />");
        };
      };
    } else {
      alertify.alert("Favor selecione uma imagem válida.");
    }
  });

});
