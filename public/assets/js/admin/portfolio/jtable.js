$(document).ready(function() {

  $('#jtable').jtable({
    title: 'Listagem',
    paging: true,
    pageSize: 10,
    sorting: true,
    defaultSorting: 'id DESC',
    actions: {
      listAction: window.location.href + "/listing"
    },
    fields: {
      btnEdit: {
        width: '1',
        sorting: false,
        display: function(data) {
          return $("<a title='Editar o registro' class='btn btn-default btn-circle tooltipster' href='" + window.location.href + "/edit/" + data.record.id + "'><i class='fa fa-edit'></i></a>");
        }
      },
      btnDelete: {
        width: '1',
        sorting: false,
        display: function(data) {
          return $("<a title='Excluir o registro' class='btn btn-default btn-circle tooltipster btn-delete' href='" + window.location.href + "/delete/" + data.record.id + "'><i class='fa fa-times'></i></a>");
        }
      },
      id: {
        title: 'Código',
        key: true,
        width: '1',
        list: true
      },
      image: {
        title: 'Imagem',
        width: '10%',
        display: function(data) {
          return "<img width='100' src='" + window.assets + "/images/site/portfolio/" +data.record.image + "'/>";
        }
      },
      title: {
        title: 'Titulo',
        width: '40%'
      },
      link: {
        title: 'Link',
        width: '40%'
      },
      active: {
        title: 'Habilitado?',
        width: '1',
        sorting: false,
        display: function(data) {
          if (data.record.active == 'Y') {
            return $("<a title='Desabilitar o registro' class='btn btn-success btn-circle tooltipster btn-enable' href='" + window.location.href + "/active/" + data.record.id + "'>Sim</a>");
          } else {
            return $("<a title='Habilitar o registro' class='btn btn-danger btn-circle tooltipster btn-enable' href='" + window.location.href + "/active/" + data.record.id + "'>Não</a>");
          }
        }
      }
    }
  });
  $('#jtable').jtable('load');

});
