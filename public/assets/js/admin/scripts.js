$(document).ready(function() {

  $(document).ajaxStop(function() {
    $(".btn-delete").off('click').click(function(e) {
      e.preventDefault();
      var href = $(this).attr('href');
      alertify.confirm("Deseja realmente excluir o registro?", function(e) {
        if (e) {
          $.ajax({
            type: 'POST',
            url: href,
            dataType: 'json'
          }).done(function(response) {
            alertify.alert(response.msg, function() {
              if (e && response.status) {
                $('#jtable').jtable('load');
              }
            });
          });
        }
      });
    });

    $(".btn-enable").off('click').click(function(e) {
      e.preventDefault();
      var href = $(this).attr('href');
      var title = $(this).attr('title');
      alertify.confirm("Deseja realmente " + title + "?", function(e) {
        if (e) {
          $.ajax({
            type: 'POST',
            url: href,
            dataType: 'json'
          }).done(function(response) {
            alertify.alert(response.msg, function() {
              if (e && response.status) {
                $('#jtable').jtable('load');
              }
            });
          });
        }
      });
    });
  });

});