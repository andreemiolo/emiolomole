// prepare the form when the DOM is ready 
$(document).ready(function() {
  var options = {
    success: showResponse, // post-submit callback
    type: 'post', // 'get' or 'post', override for form's 'method' attribute
    dataType: 'json' // 'xml', 'script', or 'json' (expected server response type)
  };
  // bind to the form's submit event 
  $('#formLogin').submit(function() {
    $(this).ajaxSubmit(options);
    return false;
  });
});

// post-submit callback 
function showResponse(responseText, statusText, xhr, $form) {
  if (!responseText.status) {
    alertify.alert(responseText.msg);
  } else {
    location.reload();
  }
} 