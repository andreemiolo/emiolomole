<?php

$environment = App::environment();

return array(
    /*
      |--------------------------------------------------------------------------
      | API Keys
      |--------------------------------------------------------------------------
      |
      | Set the public and private API keys as provided by reCAPTCHA.
      |
     */
    // Local : Produção : Homologação
    'public_key' => ($environment == 'local') ? '6LcLJf0SAAAAAMmNdGq8Br8j8YsVzPOO-iTnB4gQ' : (($environment == 'production') ? '6LcYaP4SAAAAAL4f5cJ3SNPAP-WfczDgFvHMquN9' : '6LdGB_8SAAAAAH0AKtbycoymjuYxA02uVxGZrCli'),
    'private_key' => ($environment == 'local') ? '6LcLJf0SAAAAADFuTdBX-7Zlh5HCG5XZcc0EJG7-' : (($environment == 'production') ? '6LcYaP4SAAAAAMR_fzMyc0aKh0yyhY_iI9tWocbS' : '6LdGB_8SAAAAAMF3qWXxxsMysQEfz_ULU8E9QCMh'),
    /*
      |--------------------------------------------------------------------------
      | Template
      |--------------------------------------------------------------------------
      |
      | Set a template to use if you don't want to use the standard one.
      |
     */
    'template' => '',
    'option' => [
        'theme' => 'clean'
    ]
);
