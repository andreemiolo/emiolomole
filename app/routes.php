<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

/* * *****************************************************************************
 * ROUTE ADMIN
 * ***************************************************************************** */
Route::group(array('prefix' => 'admin', 'before' => 'auth|permissionAccessRoute'), function() {
    Route::controller('home', 'admin\HomeController');
    Route::controller('portfolio', 'admin\PortfolioController');
    Route::controller('services', 'admin\ServicesController');
    Route::post('clients/activate/{id}', 'admin\ClientsController@activate');
    Route::post('clients/delete/{id}', 'admin\ClientsController@delete');
    Route::post('clients/ordain', 'admin\ClientsController@ordain');
    Route::post('clients/save', 'admin\ClientsController@save');
    Route::get('clients', 'admin\ClientsController@index');
});

Route::match(array('GET', 'POST'), 'busca-cep/{cep?}', function($cep = false) {
    if (!$cep)
        $cep = Input::get('cep', false);

    return \helpers\Helpers::getCep($cep);
});
Route::controller('admin/login', 'admin\LoginController');
Route::controller('admin', 'admin\IndexController');

/* * *****************************************************************************
 * ROUTE SITE
 * ***************************************************************************** */
Route::controller('contact', 'site\ContactController');
Route::controller('/', 'site\IndexController');
