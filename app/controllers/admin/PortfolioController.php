<?php

namespace admin;

class PortfolioController extends \AdminController {

  public function getIndex() {
    return $this->view('portfolio.index');
  }

  public function getNew() {
    return $this->view('portfolio.new');
  }

  public function getEdit($id) {
    $this->model = \Portfolio::find($id);
    return $this->view('portfolio.edit');
  }

  public function postSave() {
    $id = \Input::get('id');
    $all_input = \Input::all();
    $image = \Input::file('image');

    $validator = \Validator::make(
                    $all_input, \Portfolio::rules($id)
    );
    if ($validator->fails()) {
      $messages = $validator->messages();
      $msg = "";
      foreach ($messages->all(':message<br/>') as $value) {
        $msg .= $value;
      }

      return $this->json(array(
                  'status' => false,
                  'msg' => $msg
      ));
    }

    if (!empty($id)) {
      $model = \Portfolio::find($id);
      $file_name = $model->image;

      if (!empty($image) && \File::isFile(public_path("$model->_path_image$model->image"))) {
        \File::delete(public_path("$model->_path_image$model->image"));
      }
    } else {
      $model = new \Portfolio();
    }

    $model->fill($all_input);

    if (!empty($image)) {
      $file_name = \helpers\Helpers::uploadImage($model->_path_image, $image);
    }

    $model->image = $file_name;

    if ($model->save()) {
      return $this->json(array(
                  'status' => true,
                  'msg' => "Registro salvo com sucesso."
      ));
    } else {
      return $this->json(array(
                  'status' => false,
                  'msg' => "Possível erro técnico, tente novamente."
      ));
    }
  }

  public function postActive($id) {
    $model = \Portfolio::find($id);
    $model->active = ($model->active == 'Y') ? 'N' : 'Y';

    if ($model->save()) {
      return $this->json(array(
                  'status' => true,
                  'msg' => "Registro salvo com sucesso."
      ));
    } else {
      return $this->json(array(
                  'status' => false,
                  'msg' => "Possível erro técnico, tente novamente."
      ));
    }
  }

  public function postDelete($id) {
    $model = \Portfolio::find($id);
    $_path_image = $model->_path_image;
    $image = $model->image;

    if ($model->delete()) {
      if (\File::isFile(public_path("$_path_image$image"))) {
        \File::delete(public_path("$_path_image$image"));
      }

      return $this->json(array(
                  'status' => true,
                  'msg' => "Registro excluído com sucesso."
      ));
    } else {
      return $this->json(array(
                  'status' => false,
                  'msg' => "Possível erro técnico, tente novamente."
      ));
    }
  }

  public function postListing() {
    $pagina = \Input::get('jtStartIndex');
    $limite = \Input::get('jtPageSize');
    $orderby = explode(" ", \Input::get('jtSorting'));

    $array = [
        'pagina' => $pagina,
        'limite' => $limite,
        'field' => $orderby[0],
        'order' => $orderby[1],
    ];
    $rs = \Portfolio::listing($array);
    return $this->json($rs);
  }

}
