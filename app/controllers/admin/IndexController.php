<?php

namespace admin;

class IndexController extends \AdminController {

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function getIndex() {
    return $this->view('index.index');
  }
  
}
