<?php

namespace admin;

class UserController extends \AdminController {

  public function getIndex() {
    $this->groups = \Group::lists('name', 'id');
    return $this->view('user.index');
  }

  public function getNew() {
    $this->groups = \Group::lists('name', 'id');
    return $this->view('user.new');
  }

  public function getEdit($id) {
    $this->groups = \Group::lists('name', 'id');
    $this->model = \User::find($id);
    return $this->view('user.edit');
  }

  public function postSave() {
    $id = \Input::get('id');
    $password = \Input::get('password');
    $all_input = \Input::all();

    $validator = \Validator::make(
                    $all_input, \User::rules($id, $password)
    );
    if ($validator->fails()) {
      $messages = $validator->messages();
      $msg = "";
      foreach ($messages->all(':message<br/>') as $value) {
        $msg .= $value;
      }

      return $this->json(array(
                  'status' => false,
                  'msg' => $msg
      ));
    }

    if (!empty($id)) {
      $model = \User::find($id);
    } else {
      $model = new \User();
    }

    $model->fill($all_input);
    $model->password = ($password) ? \Hash::make($password) : $model->password;

    if ($model->save()) {
      return $this->json(array(
                  'status' => true,
                  'msg' => "Registro salvo com sucesso."
      ));
    } else {
      return $this->json(array(
                  'status' => false,
                  'msg' => "Possível erro técnico, tente novamente."
      ));
    }
  }

  public function postDelete($id) {
    $model = \User::find($id);

    if ($model->delete()) {
      return $this->json(array(
                  'status' => true,
                  'msg' => "Registro excluído com sucesso."
      ));
    } else {
      return $this->json(array(
                  'status' => false,
                  'msg' => "Possível erro técnico, tente novamente."
      ));
    }
  }

  public function postListing() {
    $pagina = \Input::get('jtStartIndex');
    $limite = \Input::get('jtPageSize');
    $orderby = explode(" ", \Input::get('jtSorting'));
    $orderby = array('field' => $orderby[0], 'order' => $orderby[1]);

    $rs = \User::with('group')
            ->where(function($query) {
                      $name = \Input::get('name');
                      if (!empty($name)) {
                        $query->where('name', 'LIKE', "%$name%");
                      }
                    })
            ->orderBy($orderby['field'], $orderby['order']);
    $count = $rs->count();
    $rs = $rs->skip($pagina)
            ->take($limite)
            ->get()
            ->toArray();

    return $this->json(array(
                'Result' => "OK",
                'TotalRecordCount' => $count,
                'Records' => $rs
    ));
  }

}
