<?php

namespace admin;

class HomeController extends \AdminController {

    public function getIndex() {
        $this->bannersWeb = \HomeImages::where("type", "web")->get();
        $this->bannersMobile = \HomeImages::where("type", "mobile")->get();
        return $this->view('home.index');
    }

    public function postUploadBanner() {
        $path = "assets/images/site/home/";
        $all_input = \Input::all();
        $files = \Input::file('image');

        $validator = \Validator::make(
                        array('image' => $files, 'type' => $all_input['type']), \HomeImages::$rules
        );
        if ($validator->fails()) {
            $messages = $validator->messages();
            $msg = "";
            foreach ($messages->all(':message<br/>') as $value) {
                $msg .= $value;
            }

            return $this->json(array(
                        'status' => false,
                        'msg' => $msg
            ));
        }

        if (!\File::isDirectory(public_path("$path"))) {
            \File::makeDirectory(public_path("$path"), 777, true);
        }

        foreach ($files as $file) {
            $intervation = \Image::make($file->getRealPath());

            $getClientOriginalName = md5(date("dmYHis") . $file->getClientOriginalName());
            $getClientOriginalExtension = $file->getClientOriginalExtension();
            $name_file = "$getClientOriginalName.$getClientOriginalExtension";
            $intervation->save(public_path("$path$name_file"), 100);

            $model = new \HomeImages();

            $model->image = $name_file;
            $model->type = $all_input['type'];
            $model->save();
        }


        if ($model) {
            return $this->json(array(
                        'status' => true,
                        'msg' => "Registro salvo com sucesso."
            ));
        } else {
            return $this->json(array(
                        'status' => false,
                        'msg' => "Possível erro técnico, tente novamente."
            ));
        }
    }

    public function postDeleteBanner($id) {
        $model = \HomeImages::find($id);
        $f = $model->image;

        $path = "assets/images/site/home/";
        $url = url($path);
        $str_replace = str_replace($url . "/", '', $f);
        
        if (unlink("$path$str_replace")) {
            if ($model->delete()) {
                return $this->json(array(
                            'status' => true,
                            'msg' => "Registro excluído com sucesso."
                ));
            } else {
                return $this->json(array(
                            'status' => false,
                            'msg' => "Possível erro técnico, tente novamente."
                ));
            }
        } else {
            return $this->json(array(
                        'status' => false,
                        'msg' => "Possível erro técnico, tente novamente."
            ));
        }
    }

}
