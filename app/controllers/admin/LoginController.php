<?php

namespace admin;

class LoginController extends \AdminLoginController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex() {
        return $this->view('login.index');
    }

    public function getLogout() {
        \Auth::logout();
        return \Redirect::to('admin/login');
    }

    public function postEnter() {
        $username = \Input::get('username');
        $password = \Input::get('password');

        $validator = \Validator::make(
                        array(
                    'username' => $username
                    , 'password' => $password
                        )
                        , array(
                    'username' => "required"
                    , 'password' => "required"
                        )
        );
        if ($validator->fails()) {
            $messages = $validator->messages();
            $msg = "";
            foreach ($messages->all(':message<br/>') as $value) {
                $msg .= $value;
            }
            return $this->json(array(
                        'status' => false,
                        'msg' => $msg
            ));
        }

        if (\Auth::attempt(array('username' => $username, 'password' => $password))) {
            return $this->json(array('status' => true));
        } else {
            return $this->json(array(
                        'status' => false,
                        'msg' => "Usuário e/ou Senha incorreto(s)."
            ));
        }
    }

}
