<?php

namespace admin;

class ServicesController extends \AdminController {

  public function getIndex() {
    $this->model = \Service::first();
    return $this->view('services.index');
  }

  public function postSave() {
    $id = \Input::get('id');
    $all_input = \Input::all();

    $validator = \Validator::make(
                    $all_input, \Service::rules($id)
    );
    if ($validator->fails()) {
      $messages = $validator->messages();
      $msg = "";
      foreach ($messages->all(':message<br/>') as $value) {
        $msg .= $value;
      }

      return $this->json(array(
                  'status' => false,
                  'msg' => $msg
      ));
    }

    if (!empty($id)) {
      $model = \Service::find($id);
    } else {
      $model = new \Service();
    }

    $model->fill($all_input);

    if ($model->save()) {
      return $this->json(array(
                  'status' => true,
                  'msg' => "Registro salvo com sucesso."
      ));
    } else {
      return $this->json(array(
                  'status' => false,
                  'msg' => "Possível erro técnico, tente novamente."
      ));
    }
  }

}
