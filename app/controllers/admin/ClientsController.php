<?php

namespace admin;

class ClientsController extends \AdminController {

    public function index() {
        $this->images = \SiteClients::orderBy("order", "asc")->get();
        return $this->view('clients.index');
    }

    public function save() {
        $path = "assets/images/site/clients/";
        $files = \Input::file('image');

        $validator = \Validator::make(
                        array('image' => $files), \SiteClients::$rules
        );
        if ($validator->fails()) {
            $messages = $validator->messages();
            $msg = "";
            foreach ($messages->all(':message<br/>') as $value) {
                $msg .= $value;
            }

            return $this->json(array(
                        'status' => false,
                        'msg' => $msg
            ));
        }

        if (!\File::isDirectory($path)) {
            \File::makeDirectory($path, 777, true);
        }

        $count = \SiteClients::max('order') + 1;

        foreach ($files as $key => $file) {
            $intervation = \Image::make($file->getRealPath());

            $getClientOriginalName = md5(date("dmYHis") . $file->getClientOriginalName());
            $getClientOriginalExtension = $file->getClientOriginalExtension();
            $name_file = "$getClientOriginalName.$getClientOriginalExtension";
            $intervation->save(public_path("$path$name_file"), 100);

            $model = new \SiteClients();

            $model->image = $name_file;
            $model->order = $count + $key;
            $model->active = 'Y';
            $model->save();
        }


        if ($model) {
            return $this->json(array(
                        'status' => true,
                        'msg' => "Registro salvo com sucesso."
            ));
        } else {
            return $this->json(array(
                        'status' => false,
                        'msg' => "Possível erro técnico, tente novamente."
            ));
        }
    }

    public function ordain() {
        $ids = explode(',', \Input::get('ids'));
        $key = 1;
        foreach ($ids as $value) {
            $model = \SiteClients::find($value);
            $model->active = $model->active;
            $model->order = $key;
            $model->save();
            $key++;
        }

        if ($model) {
            return $this->json(array(
                        'status' => true,
                        'msg' => "Registro salvo com sucesso."
            ));
        } else {
            return $this->json(array(
                        'status' => false,
                        'msg' => "Possível erro técnico, tente novamente."
            ));
        }
    }

    public function activate($id) {
        $array = array('Y' => "N", 'N' => "Y");
        $model = \SiteClients::find($id);

        $model->active = $array[$model->active];

        if ($model->save()) {
            return $this->json(array(
                        'status' => true,
                        'msg' => "Operação realizada com sucesso."
            ));
        } else {
            return $this->json(array(
                        'status' => false,
                        'msg' => "Possível erro técnico, tente novamente."
            ));
        }
    }

    public function delete($id) {
        $model = \SiteClients::find($id);
        $path = "assets/images/site/clients/{$model->image}";
        if (\File::isFile($path)) {
            if (\File::delete($path)) {
                if ($model->delete()) {
                    return $this->json(array(
                                'status' => true,
                                'msg' => "Registro excluído com sucesso."
                    ));
                } else {
                    return $this->json(array(
                                'status' => false,
                                'msg' => "Possível erro técnico, tente novamente."
                    ));
                }
            } else {
                return $this->json(array(
                            'status' => false,
                            'msg' => "Possível erro técnico, tente novamente."
                ));
            }
        } else {
            if ($model->delete()) {
                return $this->json(array(
                            'status' => true,
                            'msg' => "Registro excluído com sucesso."
                ));
            } else {
                return $this->json(array(
                            'status' => false,
                            'msg' => "Possível erro técnico, tente novamente."
                ));
            }
        }
    }

}
