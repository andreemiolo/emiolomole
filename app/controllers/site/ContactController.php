<?php

namespace site;

class ContactController extends \SiteController {

  public function postSend() {
    $recaptcha_response_field = \Input::get('g-recaptcha-response');
    $all_input = \Input::all();

    $validator = \Validator::make($all_input
                    , array(
                'name' => "required",
                'email' => "required|email",
                'phone' => "required|min:14",
                'subject' => "required",
                'service' => "required_if:subject,Orçamento",
                'text' => "required",
                'file' => "required_if:subject,Trabalhe Conosco|mimes:pdf,docx,doc,jpeg,png",
                    )
    );
    if ($validator->fails()) {
      $messages = $validator->messages();
      $msg = "";
      foreach ($messages->all(':message<br/>') as $value) {
        $msg .= $value;
      }
      return $this->json(array(
                  'status' => false,
                  'msg' => $msg
      ));
    }

    $validator = \Validator::make(array('recaptcha_response_field' => $recaptcha_response_field), array('recaptcha_response_field' => 'required|recaptcha'));
    if ($validator->fails()) {
      $messages = $validator->messages();
      $msg = "";
      foreach ($messages->all(':message<br/>') as $value) {
        $msg .= $value;
      }

      return $this->json(array(
                  'status' => false,
                  'msg' => $msg
      ));
    }

    \Mail::send('emails.site.contact', $all_input, function($message) use($all_input) {
              $message->from($all_input['email'], $all_input['name'])
                      ->to('suporte@emiolo.com', 'Suporte eMiolo.com')
                      ->subject($all_input['subject'] . ' - eMiolo.com');
              $file = \Input::file('file');
              if (!empty($file) && \File::isFile($file)) {
                $message->attach($file, array('as' => $file->getClientOriginalName(), 'mime' => $file->getMimeType()));
              }
            });
    if (count(\Mail::failures()) == 0) {
      return $this->json(array(
                  'status' => true,
                  'msg' => "Contato enviado com sucesso."
      ));
    } else {
      return $this->json(array(
                  'status' => false,
                  'msg' => "Não foi possível enviar o contato, tente novamente."
      ));
    }
  }

}

