<?php

namespace site;

class IndexController extends \SiteController {

  public function getIndex() {
    return $this->view('index');
  }

}
