<?php

View::composer('site.home.index', function($view) {
    $view->with('home', "AQUI");
});

View::composer('layouts.admin', function($view) {
    $array_orute = array('new' => 'Novo', 'edit' => 'Editar', 'permission' => 'Permissão', 'gallery' => 'Galeria', 'images' => 'Imagens');
    $route1 = Request::segment(2);
    $rota2 = Request::segment(3);
    $title = "";
    $icon = "";
    if (empty($route1)) {
        $title = "Dashboard";
        $icon = "ls-ico-home";
    } else if ($route1 == "account") {
        $title = "Conta";
        $icon = "ls-ico-user";
    } else if ($route1 == "support") {
        $title = "Suporte";
        $icon = "ls-ico-cog";
    } else {
        $rota = \Menu::title($route1);
        if (isset($rota[0])) {
            $title = $rota[0]['name'];
            $icon = $rota[0]['icon'];
            if (isset($rota[0]['sub_menus'][0]) && count($rota[0]['sub_menus']) > 0) {
                $title .= " / " . $rota[0]['sub_menus'][0]['name'];
                if (!empty($rota2)) {
                    $title .= " / " . ucwords($array_orute[$rota2]);
                }
            }
        }
    }

    $view->with('icon', $icon);
    $view->with('title', $title);
});

View::composer('partials.admin.menu', function($view) {
    if (\Auth::check()) {
        $route = Request::segment(2);
        $query = \Menu::menus();

        $rota = \Menu::title($route);
        $active = "";
        if (isset($rota[0])) {
            $active = $rota[0]['name'];
        }
        $view->with('menu_active', $active);
        $view->with('menu', $query);
        $view->with('nome_usuario', \Auth::user()->name);
    }
});

View::composer('site.portfolio.index', function($view) {
    $result = \Portfolio::getActives();
    $count = $result->count();
    $ten = ceil($count / 10);
    $portifolios = array();
    if ($ten >= 1) {
        for ($index = 0; $index < $ten; $index++) {
            $items = array();
            foreach ($result as $key => $value) {
                if (count($items) <= 9) {
                    $items[] = $value;
                    unset($result[$key]);
                }
            }
            $portifolios[] = $items;
        }
    }
    
    $view->with('portfolios', $portifolios);
});

View::composer('site.home.index', function($view) {
    $result = \HomeImages::allWebSiteImages();
    $view->with('website_imgs', $result);
    $result = \HomeImages::allAppImages();
    $view->with('app_imgs', $result);
});

View::composer('site.services.index', function($view) {
    $result = \Service::getServices();
    $view->with('services', $result);
});

View::composer('site.clients.index', function($view) {
    $result = \SiteClients::allActiveImages();
    $view->with('clients', $result);
});

View::composer('site.contact.index', function($view) {
    $environment = \App::environment();
    $view->with('environment', (($environment == 'local') ? '6LcLJf0SAAAAAMmNdGq8Br8j8YsVzPOO-iTnB4gQ' : '6LcYaP4SAAAAAL4f5cJ3SNPAP-WfczDgFvHMquN9'));
});
