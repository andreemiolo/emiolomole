@section('content')

<div class="row">
  <div class="col-lg-12">
    <p>
      <a href="{{ url('admin/portfolio') }}" class="btn btn-default">Voltar</a>
      <a href="{{ url('admin/portfolio/new') }}" class="btn btn-success">Novo</a>
    </p>
  </div>
  <!-- /.col-lg-12 -->
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        Cadastro
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-lg-6">
            {{ Form::model($model, array('url' => array('admin/portfolio/save'), 'id' => 'form', 'files' => true)) }}
            {{ Form::hidden('id', null, ['id' => 'id']) }}

            <div class="form-group">
              <label>Título</label>
              {{ Form::text('title', null, [
              'class' => 'form-control',
              'placeholder' => 'Título',
              'maxlength' => '150'
              ]); }}
            </div>

            <div class="form-group">
              <label>Link</label>
              {{ Form::text('link', null, [
              'class' => 'form-control',
              'placeholder' => 'Link',
              'maxlength' => '150'
              ]); }}
            </div>

            <div class="form-group">
              <label>Descrição</label>
              {{ Form::textarea('description', null, [
              'class' => 'form-control',
              'placeholder' => 'Descrição'
              ]); }}
            </div>

            <div class="form-group">
              <label>Imagem</label>
              {{ Form::file('image', [
              'id' => 'image',
              'class' => 'file'
              ]) }}
            </div>
            <div id="preview-image"><img width="450" src="{{ url($model->_path_image.$model->image) }}"/></div>

            <div class="form-group">
              <label>Habilitar</label>
              {{ Form::select('active', [
              'Y' => 'Sim',
              'N' => 'Não',
              ], null, [
              'class' => 'form-control'
              ]); }}
            </div>

            {{ Form::submit('Salvar', [
            'class' => 'btn btn-default'
            ]); }}
            {{ Form::reset('Limpar', [
            'class' => 'btn btn-default'
            ]); }}

            {{ Form::close() }}            
          </div>          
          <!-- /.col-lg-6 (nested) -->          
        </div>
        <!-- /.row (nested) -->
      </div>
      <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
  </div>
  <!-- /.col-lg-12 -->
</div>
@stop

@section('js')
<script src="{{ url('/assets/js/admin/portfolio/form.js') }}"></script>
<script src="{{ url('/assets/js/admin/portfolio/index.js') }}"></script>
@stop