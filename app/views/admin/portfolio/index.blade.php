@section('content')

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <p><a href="{{ url('admin/portfolio/new') }}" class="btn btn-default">Novo</a></p>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-lg-12">
            <div id="jtable"></div>
          </div>          
          <!-- /.col-lg-6 (nested) -->          
        </div>
        <!-- /.row (nested) -->
      </div>
      <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
  </div>
  <!-- /.col-lg-12 -->
</div>
@stop

@section('js')
<script src="{{ URL::asset('/assets/js/admin/portfolio/jtable.js') }}"></script>
@stop