@section('content')

<div class="ls-box-filter">
  <a class="ls-btn-primary ls-ico-plus" href="{{ url('admin/group/new') }}">Novo</a>
</div>

<div class="ls-box ls-board-box">
  <header class="ls-info-header">
    <h2 class="ls-title-3">Filtros de Busca</h2>
  </header>

  <div class="row" id="sending-stats">
    <div class="col-sm-12">
      {{ Form::open(array('url' => array('admin/group/listing'), 'id' => 'formSearch', 'class' => 'ls-form ls-form-horizontal row')) }}
      <fieldset>
        <label class="ls-label col-md-3">
          <b class="ls-label-text">Nome</b>
          {{ Form::text('name', null, [
          'class' => 'ls-field'
          ]) }}
        </label>
      </fieldset>

      <div class="ls-actions-btn">
        {{ Form::submit('Pesquisar', [
        'class' => 'ls-btn',
        'id' => 'btnSearch'
        ]) }}
        {{ Form::reset('Limpar', [
        'class' => 'ls-btn-danger',
        'id' => 'btnClean'
        ]) }}
      </div>

      {{ Form::close() }}
    </div>
  </div>
</div>

<div class="ls-box ls-board-box">
  <header class="ls-info-header">
    <h2 class="ls-title-3">Listagem</h2>
  </header>

  <div class="row" id="sending-stats">
    <div class="col-sm-12">
      <div id="jtable"></div>
    </div>
  </div>
</div>


@stop

@section('js')
<script src="{{ url('/assets/js/admin/group/jtable.js') }}"></script>
<script src="{{ url('/assets/js/admin/group/index.js') }}"></script>
@stop