@section('content')

<div class="ls-box-filter">
  <a class="ls-btn-dark ls-ico-list3" href="{{ url('admin/group') }}">Listagem</a>
</div>

<div class="ls-box ls-board-box">
  <header class="ls-info-header">
    <h2 class="ls-title-3">Cadastro</h2>
  </header>

  <div class="row" id="sending-stats">
    <div class="col-sm-12">
      {{ Form::open(array('url' => array('admin/group/save'), 'id' => 'form', 'class' => 'ls-form row')) }}
      <fieldset>
        <label class="ls-label col-md-4 col-xs-12">
          <b class="ls-label-text">Nome</b>
          {{ Form::text('name', null, [
          'class' => 'ls-field'
          ]) }}
        </label>

        <label class="ls-label col-md-4 col-xs-12">
          <b class="ls-label-text">Status</b>
          <div data-ls-module="customFields">
            {{ Form::select('active', [
            '1' => 'Habilitar',
            '0' => 'Desabilitar',
            ], null, [
            'class' => 'go-to-option'
            ]) }}
          </div>
        </label>
      </fieldset>

      <div class="ls-actions-btn">
        {{ Form::submit('Salvar', [
        'class' => 'ls-btn'
        ]) }}
        {{ Form::reset('Limpar', [
        'class' => 'ls-btn-danger'
        ]) }}
      </div>
      {{ Form::close() }}
    </div>
  </div>
</div>

@stop

@section('js')
<script src="{{ url('/assets/js/admin/group/form.js') }}"></script>
@stop