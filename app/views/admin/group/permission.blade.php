@section('content')

<div class="ls-box-filter">
  <a class="ls-btn-dark ls-ico-list3" href="{{ url('admin/group') }}">Listagem</a>
</div>

<div class="ls-box ls-board-box">
  <header class="ls-info-header">
    <h2 class="ls-title-3">Cadastro</h2>
  </header>

  <div class="row" id="sending-stats">
    <div class="col-sm-3">
      {{ Form::open(array('url' => array('admin/group/save-permission'), 'id' => 'form', 'class' => 'ls-form row')) }}
      {{ Form::hidden('id', $id) }}
      <fieldset>
        <table class="ls-table">
          <thead>
            <tr>
              <th width='1'>#</th>
              <th>Submenu</th>
            </tr>
          </thead>
          <tbody>
            @foreach($sub_menus as $value)
            <tr>
              <td>
                {{ Form::checkbox('permission[]', $value->id, (count($value->groups)) ? true : false) }}
              </td>
              <td>{{ $value->name }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </fieldset>

      <div class="ls-actions-btn">
        {{ Form::submit('Salvar', [
        'class' => 'ls-btn'
        ]) }}
        {{ Form::reset('Limpar', [
        'class' => 'ls-btn-danger'
        ]) }}
      </div>
      {{ Form::close() }}
    </div>
  </div>
</div>

@stop

@section('js')
<script src="{{ url('/assets/js/admin/group/permission.js') }}"></script>
@stop