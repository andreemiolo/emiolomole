@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-4 col-md-offset-4">
      <div class="login-panel panel panel-default">
        <div class="panel-heading">
          <img title="Login" src="{{ url('assets/images/admin/logo_emiolo.png') }}" />
        </div>
        <div class="panel-body">
          {{ Form::open(array('url' => 'admin/login/enter', 'method' => 'post', 'id' => 'formLogin', 'role' => 'form')) }}
          <fieldset>
            <div class="form-group">
              {{ Form::text('username', '', ['class' => 'form-control',
              'id' => 'username',
              'placeholder' => 'Usuário',
              'autofocus'
              ]); }}
            </div>
            <div class="form-group">
              {{ Form::password('password', ['class' => 'form-control',
              'id' => 'password',
              'placeholder' => 'Senha'
              ]); }}
            </div>
            {{ Form::submit('Entrar', [
            'class' => 'btn btn-lg btn-primary btn-block'
            ]); }}
          </fieldset>
          {{ Form::close() }}
          <p class="ls-txt-center ls-login-signup"><a target="_blank" href="http://emiolo.com">eMiolo.com</a></p>
        </div>
      </div>
    </div>
  </div>
</div>

@stop

@section('js')
<script src="{{ url('assets/js/admin/login/index.js') }}"></script>
@stop