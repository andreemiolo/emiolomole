@section('css')
<style type="text/css">
    #sortable { list-style-type: none; margin: 0; padding: 0; width: 100%; }
    #sortable li { display: inline-block; border: 1px solid #CCC; background-color: #F0F0F0; padding: 10px; width: 200px; height: 200px; }
    #sortable li p { text-align: center; width: 180px; height: 130px; display: block; position: relative; }

</style>
@stop

@section('content')

<div class="row">
    <div class="col-lg-12">
        <p>
            <a href="{{ url('admin/clients') }}" class="btn btn-default">Atualizar</a>
        </p>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Cadastro
            </div>
            <div class="panel-body">

                <div class="panel-body row">
                    <div class="col-lg-12">
                        {{ Form::open(array('url' => array('admin/clients/save'), 'id' => 'form-upload', 'class' => 'ls-form row form-upload', 'files' => true)) }}
                        <div class="form-group">
                            <label>Imagem (máximo 2 MB)</label>
                            {{ Form::file('image[]', [
                                        'id' => 'image',
                                        'class' => 'file',
                                        'multiple' => ''
                                        ]) }}
                        </div>

                        <div class="ls-actions-btn">
                            {{ Form::submit('Salvar', [
                                    'class' => 'btn btn-default'
                                    ]) }}
                            {{ Form::reset('Limpar', [
                                    'class' => 'btn btn-primary'
                                    ]) }}
                        </div>
                        {{ Form::close() }}
                        <br>
                        <ul id="sortable">
                            @foreach($images as $value)
                            <li id="{{ $value->id }}">
                                <p><img style="max-width: 100%; max-height: 100%; position: absolute; top: 0; left: 0; right: 0; bottom: 0; margin: auto;" src="{{ $value->file }}"/></p>
                                <p>
                                    {{ Form::button('Excluir', [
                                        'class' => 'btn btn-danger delete-file',
                                        'href' => url('admin/clients/delete/' . $value->id)
                                    ]) }}
                                    {{ Form::button(($value->active == 'Y') ? 'Desativar' : 'Ativar', [
                                        'class' => 'btn btn-warning activate-file',
                                        'href' => url('admin/clients/activate/' . $value->id)
                                    ]) }}                        
                                </p>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('js')
<script src="{{ url('/assets/js/admin/clients/form.js') }}"></script>
<script src="{{ url('/assets/js/admin/clients/index.js') }}"></script>
@stop