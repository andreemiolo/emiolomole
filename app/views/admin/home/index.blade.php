@section('content')

<div class="row">
    <div class="col-lg-12">
        <p>
            <a href="{{ url('admin/home') }}" class="btn btn-default">Atualizar</a>
        </p>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Cadastro
            </div>
            <div class="panel-body">

                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#web">Banners Web</a></li>
                    <li><a data-toggle="tab" href="#mobile">Banners Mobile</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- TAB Banners Web -->
                    <div id="web" class="tab-pane fade in active">
                        <div class="panel-body row">
                            <div class="col-lg-6">
                                {{ Form::open(array('url' => array('admin/home/upload-banner'), 'id' => 'form-upload', 'class' => 'ls-form row form-upload', 'files' => true)) }}
                                {{ Form::hidden('type', 'web') }}
                                <div class="form-group">
                                    <label>Imagem (máximo 2 MB)</label>
                                    {{ Form::file('image[]', [
                                        'id' => 'image',
                                        'class' => 'file',
                                        'multiple' => ''
                                        ]) }}
                                </div>

                                <div class="ls-actions-btn">
                                    {{ Form::submit('Salvar', [
                                    'class' => 'btn btn-default'
                                    ]) }}
                                    {{ Form::reset('Limpar', [
                                    'class' => 'btn btn-primary'
                                    ]) }}
                                </div>
                                {{ Form::close() }}
                                <br>
                                @foreach($bannersWeb as $value)
                                <fieldset>
                                    <table class="table ">
                                        <tbody>              
                                            <tr>                
                                                <td><img style="width:200px; height: 100px;" src="{{ $value->file }}"/></td>
                                                <td>
                                                    <div>
                                                        {{ Form::button('Excluir', [
                                                        'class' => 'btn btn-danger delete-file',
                                                        'href' => url('admin/home/delete-banner/' . $value->id)
                                                        ]) }}
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </fieldset>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- TAB Banners Mobile-->
                    <div id="mobile" class="tab-pane">
                        <div class="panel-body row">
                            <div class="col-lg-6">
                                {{ Form::open(array('url' => array('admin/home/upload-banner'), 'id' => 'form-upload', 'class' => 'ls-form row form-upload', 'files' => true)) }}
                                {{ Form::hidden('type', 'mobile') }}
                                <div class="form-group">
                                    <label>Imagem (máximo 2 MB)</label>
                                    {{ Form::file('image[]', [
                                                'id' => 'image',
                                                'class' => 'file',
                                                'multiple' => ''
                                                ]) }}
                                </div>

                                <div class="ls-actions-btn">
                                    {{ Form::submit('Salvar', [
                                                'class' => 'btn btn-default'
                                                ]) }}
                                    {{ Form::reset('Limpar', [
                                                'class' => 'btn btn-primary'
                                                ]) }}
                                </div>
                                {{ Form::close() }}

                                @foreach($bannersMobile as $value)
                                <fieldset>
                                    <table class="table">
                                        <tbody>              
                                            <tr>                
                                                <td><img style="width: 142; height: 250px;" src="{{ $value->file }}"/></td>
                                                <td>
                                                    <div class="ls-group-btn ls-group-active">
                                                        {{ Form::button('Excluir', [
                                                        'class' => 'btn btn-danger delete-file',
                                                        'href' => url('admin/home/delete-banner/' . $value->id)
                                                        ]) }}
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </fieldset>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('js')
<script src="{{ url('/assets/js/admin/home/form.js') }}"></script>
<script src="{{ url('/assets/js/admin/home/index.js') }}"></script>
@stop