@section('content')

<div class="row">
  <div class="col-lg-12">
    <p>
      <a href="{{ url('admin/services') }}" class="btn btn-default">Atualizar</a>
    </p>
  </div>
  <!-- /.col-lg-12 -->
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        Cadastro
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-lg-6">
            {{ Form::model($model, array('url' => array('admin/services/save'), 'id' => 'form')) }}
            {{ Form::hidden('id', null, ['id' => 'id']) }}

            <div class="form-group">
              <label>Web Sites</label>
              {{ Form::text('website', null, [
              'class' => 'form-control',
              'placeholder' => 'Web Sites',
              'maxlength' => '80'
              ]) }}
              <p class="help-block">máximo 80 caracteres.</p>
            </div>

            <div class="form-group">
              <label>Aplicativos</label>
              {{ Form::text('app', null, [
              'class' => 'form-control',
              'placeholder' => 'Aplicativos',
              'maxlength' => '80'
              ]) }}
              <p class="help-block">máximo 80 caracteres.</p>
            </div>

            <div class="form-group">
              <label>Sistemas</label>
              {{ Form::text('software', null, [
              'class' => 'form-control',
              'placeholder' => 'Sistemas',
              'maxlength' => '80'
              ]) }}
              <p class="help-block">máximo 80 caracteres.</p>
            </div>

            {{ Form::submit('Salvar', [
            'class' => 'btn btn-default'
            ]) }}
            {{ Form::reset('Limpar', [
            'class' => 'btn btn-default'
            ]) }}

            {{ Form::close() }}            
          </div>          
          <!-- /.col-lg-6 (nested) -->          
        </div>
        <!-- /.row (nested) -->
      </div>
      <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
  </div>
  <!-- /.col-lg-12 -->
</div>
@stop

@section('js')
<script src="{{ url('/assets/js/admin/services/form.js') }}"></script>
@stop