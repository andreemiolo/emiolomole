<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>eMiolo.com - Cerebelo</title>

    <link rel="shortcut icon" href="{{ url('assets/images/admin/favicon.png') }}" type="image/x-icon">

    <!-- Bootstrap Core CSS -->
    <link href="{{ url('assets/css/admin/bootstrap.min.css') }}" rel="stylesheet">

    <!-- jQueryUi CSS -->
    <link href="{{ url('assets/plugins/jquery-ui-1.11.1/jquery-ui.min.css') }}" rel="stylesheet"/>

    <!-- jTable CSS -->
    <link href="{{ url('assets/plugins/jtable.2.4.0/themes/metro/lightgray/jtable.min.css') }}" rel="stylesheet"/>

    <link href="{{ url('assets/plugins/alertify/alertify.core.css') }}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{ url('assets/css/admin/plugins/metisMenu/metisMenu.min.css') }}" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="{{ url('assets/css/admin/plugins/timeline.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ url('assets/css/admin/sb-admin-2.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ url('assets/font-awesome-4.1.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('css')
  </head>

  <body>

    <div id="wrapper">
      @include('partials.admin.menu')
      <div id="page-wrapper">
        @include('partials.admin.title')

        @yield('content')
        <div class="navbar navbar-default"><p class="text-center text-muted" style="margin: 14px;">&copy; Copyright {{ date("Y")}} www.emiolo.com - Todos os Direitos Reservados</p></div>
      </div>
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="{{ url('assets/js/admin/jquery.js') }}"></script>
    <script type="text/javascript">
      window.url = "{{ url('admin') }}";
      window.assets = "{{ url('assets') }}";
    </script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ url('assets/js/admin/bootstrap.min.js') }}"></script>

    <!-- jQueryUi JS -->
    <script src="{{ url('assets/plugins/jquery-ui-1.11.1/jquery-ui.min.js') }}"></script>

    <!-- blockUI JS -->
    <script src="{{ url('assets/plugins/jquery.blockUI.js') }}"></script>

    <!-- jTable JS -->
    <script src="{{ url('assets/plugins/jtable.2.4.0/jquery.jtable.js') }}"></script>

    <script src="{{ url('assets/plugins/jquery.form.min.js') }}"></script>

    <script src="{{ url('assets/plugins/alertify/alertify.js') }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ url('assets/js/admin/plugins/metisMenu/metisMenu.min.js') }}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{ url('assets/js/admin/sb-admin-2.js') }}"></script>

    <script src="{{ url('assets/js/admin/scripts.js') }}"></script>
    @yield('js')
  </body>

</html>
