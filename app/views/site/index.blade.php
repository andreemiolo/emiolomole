@section('content')

@include('site.home.index')
@include('site.portfolio.index')
@include('site.clients.index')
@include('site.services.index')
@include('site.map.index')
@include('site.contact.index')

@stop
