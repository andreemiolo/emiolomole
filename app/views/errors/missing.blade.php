<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>eMiolo.com | Inteligência em Internet - Página não encontrada</title>

    <link rel="shortcut icon" href="{{ url('assets/images/site/favicon.png') }}" type="image/x-icon">
    {{--*/ $img = url('assets/images/site/logo_erro.png'); /*--}}
    <style>
      html, body{
        height: 100%;
      }
      body{
        margin: 0;
        font-family: arial;
        background: url("{{ $img }}") no-repeat bottom right #932d2d;
        background-size: 50%;

        /* flexbox, por favor 
        display: -webkit-box;
        -webkit-box-orient: horizontal;
        -webkit-box-pack: center;
        -webkit-box-align: center;
       
        display: -moz-box;
        -moz-box-orient: horizontal;
        -moz-box-pack: center;
        -moz-box-align: center;
        
        display: box;
        box-orient: horizontal;
        box-pack: center;
        box-align: center;*/
      }
      h1{
        font-weight: lighter;
        margin-top: 0;
        color: #932d2d;
      }
      #faixa{
        width: 90%;
        padding: 30px 5%;
        height: 220px;
        position: absolute;
        top: 50%;
        margin-top: -140px;
        background: #fff;
      }
      p, li{
        color: #666;
      }
    </style>

  </head>
  <body>

    <div id="faixa">

      <h1>Página não encontrada</h1>

      <p>O conteúdo pode ter sido removido ou não estar mais disponível.</p>

      <p>Você pode:</p>

      <ul>
        <li>Verificar se digitou corretamente o endereço desejado.</li>
        <li>Retornar à <a href="{{ url('/') }}">página inicial</a>.</li>
      </ul>

      <p>Se o problema persistir, entre em contato através de um dos canais de atendimento <a href="{{ url('#contact') }}">eMiolo.com</a>.</p>

    </div>

  </body>
</html>
