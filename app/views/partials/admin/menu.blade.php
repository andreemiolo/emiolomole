@if(\Auth::check())
<nav style="margin-bottom: 0" role="navigation" class="navbar navbar-default navbar-static-top">
    <div class="navbar-header">
        <a href="{{ url('') }}" class="navbar-brand"><img src="{{ url('assets/images/admin/logo_cerebelo.png') }}"/></a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <span class="navbar-brand">Olá, {{ $nome_usuario }}</span>
        <li class="dropdown">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="#"><i class="fa fa-user fa-fw"></i> Meus Dados</a>
                </li>
                <li><a href="#"><i class="fa fa-gear fa-fw"></i> Alterar Senha</a>
                </li>
                <li class="divider"></li>
                <li><a href="{{ url('admin/login/logout') }}"><i class="fa fa-sign-out fa-fw"></i> Sair</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    
    <!-- /.navbar-top-links -->
    <div role="navigation" class="navbar-default sidebar">
        <div class="sidebar-nav navbar-collapse">
            <ul id="side-menu" class="nav">
                <li class="sidebar-search">
                    <div class="input-group custom-search-form">
                        <img src="{{ url('assets/images/admin/logo.png') }}"/>
                    </div>
                </li>
                <li class="{{ ($menu_active == '') ? 'active' : '' }}">
                    <a href="{{ url('admin') }}"><i class="fa fa-dashboard fa-fw"></i> Início</a>
                </li>
                @foreach($menu as $v)
                @if(count($v['sub_menus']) > 0)
                @if(isset($v['sub_menus'][0]) && count($v['sub_menus'][0]['groups']) > 0)
                <li class="{{ ($menu_active == $v['name']) ? 'active' : '' }}">
                    <a href="#"><i class="fa fa-bar-chart-o {{ $v['icon'] }}"></i> {{ $v['name'] }}<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse {{ ($menu_active == $v['name']) ? 'in' : '' }}">
                        @foreach($v['sub_menus'] as $k)
                        @if(count($k['groups']) > 0)
                        <li>
                            <a class="" href="{{ url('admin/' . $k['controller']) }}"> {{ $k['name'] }}</a>
                        </li>
                        @endif
                        @endforeach
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                @endif
                @endif
                @endforeach
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>
@endif