<!-- ************* -->
<!-- MENU -->
<!-- ************* -->
<header role="banner" class="navbar navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button data-toggle="collapse-side" data-target=".side-collapse" data-target-2=".side-collapse-container" type="button" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
            </button>
        </div>
        <a href="#" id="logo">
            <img src="{{ url('assets/images/site/logo2.png') }}">
        </a>
        <div class="side-collapse in">
            <nav role="navigation" class="navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#home" onclick="rolar_para('#home')">HOME</a>
                    </li>
                    <li><a href="#titlePortfolio" onclick="rolar_para('#titlePortfolio')">PORTFÓLIO</a>
                    </li>
                    <li><a href="#titleClients" onclick="rolar_para('#titleClients')">CLIENTES</a>
                    </li>
                    <li><a href="#titleSevices" onclick="rolar_para('#titleSevices')">SERVIÇOS</a>
                    </li>
                    <li><a href="#map" onclick="rolar_para('#map')">CONTATO</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>
<!-- ************* -->
<!-- MENU -->
<!-- ************* -->