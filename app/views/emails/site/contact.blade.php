<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>eMiolo.com | Inteligência em Internet - Contato </title>

    <link rel="shortcut icon" href="{{ url('assets/images/site/favicon.png') }}" type="image/x-icon">

    <!-- Bootstrap -->
    <link href="{{ url('assets/css/site/bootstrap.min.css') }}" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
  </head>

  <body>

    <table border="0" cellpadding="3" cellspacing="0">
      <tr>
        <td colspan="2" style="font-weight: bold; text-align: center; border-bottom: 1px solid #111111; font-size: 12pt;">Contato pelo Site</td>
      </tr>
      <tr>
        <td style="font-weight: bold; border-bottom: 1px solid #111111;">Nome:</td>
        <td style="border-bottom: 1px solid #111111;">{{ $name }}</td>
      </tr>
      <tr>
        <td style="font-weight: bold; border-bottom: 1px solid #111111;">E-mail:</td>
        <td style="border-bottom: 1px solid #111111;">{{ $email }}</td>
      </tr>
      <tr>
        <td style="font-weight: bold; border-bottom: 1px solid #111111;">Telefone:</td>
        <td style="border-bottom: 1px solid #111111;">{{ $phone }}</td>
      </tr>
      <tr>
        <td style="font-weight: bold; border-bottom: 1px solid #111111;">Assunto:</td>
        <td style="border-bottom: 1px solid #111111;">{{ $subject }}</td>
      </tr>
      @if(!empty($service))
      <tr>
        <td style="font-weight: bold; border-bottom: 1px solid #111111;">Serviço:</td>
        <td style="border-bottom: 1px solid #111111;">{{ $service }}</td>
      </tr>
      @endif
      <tr>
        <td style="font-weight: bold; border-bottom: 1px solid #111111;">Mensagem:</td>
        <td style="border-bottom: 1px solid #111111;">{{ $text }}</td>
      </tr>
    </table>
    <br/>
    <br/>
    <br/>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ url('assets/js/site/bootstrap.min.js') }}"></script>    
  </body>
</html>