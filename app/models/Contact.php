<?php

class Contact extends BaseModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'contact';
    protected $fillable = array('name', 'email', 'id_contact_type', 'budget_type', 'message', 'file');
    static $rules = [
        'name' => 'required',
        'email' => 'required|email',
        'id_contact_type' => 'required'
    ];

    public function getDates() {
        return ['created_at', 'updated_at'];
    }

}
