<?php

class Menu extends BaseModel {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'menu';
  protected $fillable = array('name', 'icon', 'active');

  public static function boot() {
    parent::boot();
    $id_group = \Auth::user()->id_group;
    static::creating(function($model) use($id_group) {
              \Cache::forget('menuCache' . $model->controller);
            });
    static::updating(function($model) use($id_group) {
              \Cache::forget('menuCache' . $model->controller);
            });
    static::deleted(function($model) use($id_group) {
              \Cache::forget('menuCache' . $model->controller);
            });
  }

  public function getDates() {
    return ['created_at', 'updated_at'];
  }

  public function subMenus() {
    return $this->hasMany('SubMenu', 'id_menu');
  }

  public static function title($route) {
    return \Cache::remember('TitleCache' . $route, 60, function() use($route) {
                      return self::with(array('subMenus' => function($query) use($route) {
                                            $query->where('controller', '=', $route);
                                          }))
                                      ->whereHas('subMenus', function($query) use($route) {
                                                $query->where('controller', '=', $route);
                                              })
                                      ->take(1)->get()->toArray();
                    });
  }

  public static function menus() {
    $id_group = \Auth::user()->id_group;
    return \Cache::remember('menuCache' . $id_group, 60, function() use($id_group) {
                      return self::with(array('subMenus' => function($query) use($id_group) {
                                            $query->with(array('groups' => function($query) use($id_group) {
                                                  $query->where('group.id', '=', $id_group);
                                                  $query->active();
                                                }))
                                            ->whereHas('groups', function($query) use($id_group) {
                                                      $query->where('group.id', '=', $id_group);
                                                      $query->active();
                                                    })->active();
                                          }))
                                      ->whereHas('subMenus', function($query) use($id_group) {
                                                $query->with(array('groups' => function($query) use($id_group) {
                                                      $query->where('group.id', '=', $id_group);
                                                      $query->active();
                                                    }))
                                                ->whereHas('groups', function($query) use($id_group) {
                                                          $query->where('group.id', '=', $id_group);
                                                          $query->active();
                                                        })->active();
                                              })
                                      ->active()
                                      ->get()->toArray();
                    });
  }

  public function scopeActive($q) {
    return $q->where("active", "=", 'Y');
  }

}

