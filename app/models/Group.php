<?php

class Group extends BaseModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'group';
    protected $fillable = array('name', 'active');
    static $rules = [
        'name' => "required|unique:group,name",
        'active' => "required"
    ];

    public function getDates() {
        return ['created_at', 'updated_at'];
    }

    public static function selectPermission() {
        $user = \Auth::user();
        return self::where(function($w) use($user) {
                    if (!in_array($user->id_group, array(1, 2))) {
                        $w->where("id", "=", $user->id_group);
                    }
                })->lists('name', 'id');
    }

    public function user() {
        return $this->belongsTo('User', 'id_group');
    }

    public function subMenus() {
        return $this->belongsToMany('SubMenu', 'submenus_group', 'id_group', 'id_submenu');
    }

    public function scopeActive($q) {
        return $q->where("active", "=", 'Y');
    }

    public static function rules($id) {
        $rules = static::$rules;
        if ($id) {
            $rules['name'] = $rules['name'] . ',' . $id;
        }

        return $rules;
    }

}
