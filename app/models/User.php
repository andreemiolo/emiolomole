<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends BaseModel implements UserInterface, RemindableInterface {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    protected $fillable = array('id_group', 'name', 'email', 'username', 'password');
    static $rules = [
        'name' => 'required',
        'email' => 'required|email',
        'id_group' => 'required',
        'password' => 'required|between6,15',
        'password_confirm' => 'required|same:password',
        'username' => 'required|unique:user,username'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    public function getDates() {
        return['created_at', 'updated_at'];
    }

    public function group() {
        return $this->belongsTo('Group', 'id_group');
    }

    public static function rules($id, $password) {
        $rules = static::$rules;
        if ($id) {
            $rules['username'] = $rules['username'] . ',' . $id;

            if (!$password) {
                unset($rules['password'], $rules['password_confirm']);
            }
        }

        return $rules;
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier() {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword() {
        return $this->password;
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken() {
        return $this->remember_token;
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value) {
        $this->remember_token = $value;
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName() {
        return 'remember_token';
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail() {
        return $this->email;
    }

}
