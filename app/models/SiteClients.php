<?php

class SiteClients extends BaseModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'site_clients';
    protected $fillable = array('image', 'order', 'active');
    static $rules = [
        'image' => "required"
    ];
    protected $appends = ['file'];

    public static function boot() {
        parent::boot();
        static::creating(function($model) {
            \Cache::forget('SiteClientsCache');
        });
        static::updating(function($model) {
            \Cache::forget('SiteClientsCache');
        });
        static::deleted(function($model) {
            \Cache::forget('SiteClientsCache');
        });
    }

    public function getDates() {
        return ['created_at', 'updated_at'];
    }

    public function getFileAttribute() {
        return url("assets/images/site/clients/" . $this->attributes['image']);
    }

    public static function allActiveImages() {
        return \Cache::remember('SiteClientsCache', 60, function() {
                    return self::where('active', 'Y')
                                    ->orderBy('order', 'asc')
                                    ->get();
                });
    }

}
