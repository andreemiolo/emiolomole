<?php

class CompanyData extends BaseModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'company_data';
    protected $fillable = array('description');
    static $rules = [
        'description' => 'required'
    ];

    public function getDates() {
        return ['created_at', 'updated_at'];
    }

    public static function rules($id) {
        $rules = static::$rules;

        return $rules;
    }

}
