<?php

class Portfolio extends BaseModel {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'portfolio';
  protected $fillable = array('title', 'link', 'description', 'image', 'active');
  static $rules = [
      'title' => "required",
      'description' => "required"
  ];
  public $_path_image = "assets/images/site/portfolio/";

  public static function boot() {
    parent::boot();
    static::creating(function($model) {
              \Cache::forget('PortfolioCache');
            });
    static::updating(function($model) {
              \Cache::forget('PortfolioCache');
            });
    static::deleted(function($model) {
              \Cache::forget('PortfolioCache');
            });
  }

  public function getDates() {
    return ['created_at', 'updated_at'];
  }

  public static function rules($id) {
    $rules = static::$rules;

    return $rules;
  }

  public static function listing($array = array()) {
    $rs = self::orderBy($array['field'], $array['order']);
    $count = $rs->count();
    $rs = $rs->skip($array['pagina'])
            ->take($array['limite'])
            ->get()
            ->toArray();

    return array(
        'Result' => "OK",
        'TotalRecordCount' => $count,
        'Records' => $rs
    );
  }

  public function scopeActive() {
    return $this->where('active', 'Y');
  }

  public function scopeOrder() {
    return $this->orderBy('id', 'desc');
  }

  public static function getActives() {
      \Cache::forget('PortfolioCache');
    return \Cache::remember('PortfolioCache', 60, function() {
                      return self::active()
                                      ->order()
                                      ->get();
                    });
  }

}
