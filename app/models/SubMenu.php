<?php

class SubMenu extends BaseModel {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'submenu';
  protected $fillable = array('id_menu', 'name', 'controller', 'active');

  public static  function boot() {
    parent::boot();
    static::creating(function($model) {
              \Cache::forget('routeCache' . $model->controller);
              \Cache::forget('TitleCache' . $model->controller);
            });
    static::updating(function($model) {
              \Cache::forget('routeCache' . $model->controller);
              \Cache::forget('TitleCache' . $model->controller);
            });
    static::deleted(function($model) {
              \Cache::forget('routeCache' . $model->controller);
              \Cache::forget('TitleCache' . $model->controller);
            });
  }

  public function getDates() {
    return ['created_at', 'updated_at'];
  }

  public function menus() {
    return $this->belongsTo('Menu', 'id_menu');
  }

  public function groups() {
    return $this->belongsToMany('Group', 'submenus_group', 'id_submenu', 'id_group');
  }

  public static function permissionAccessRoute($route) {
    return \Cache::remember('routeCache' . $route, 60, function() use($route) {
                      return self::with(array('groups' => function($query) use($route) {
                                    $query->where('submenus_group.id_group', '=', \Auth::user()->id_group);
                                  }))->where('submenu.controller', '=', $route)->active()->take('Y')->get();
                    });
  }

  public function scopeActive($q) {
    return $q->where("active", "=", 'Y');
  }

  public function scopePermission($q, $id_group) {
    return $q->whereHas('groups', function($query) use($id_group) {
                      $query->where('submenus_group.id_group', '=', $id_group);
                    });
  }

}
