<?php

class ContactType extends BaseModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'contact_type';
    protected $fillable = array('name', 'active');
    static $rules = [
        'name' => 'required'
    ];

    public function getDates() {
        return ['created_at', 'updated_at'];
    }

    public function scopeActive($q) {
        return $q->where("active", "=", 'Y');
    }

    public static function rules($id) {
        $rules = static::$rules;

        return $rules;
    }

}
