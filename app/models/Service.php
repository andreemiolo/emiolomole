<?php

class Service extends BaseModel {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'service';
  protected $fillable = array('website', 'app', 'software');
  static $rules = [
      'website' => "required",
      'app' => "required",
      'software' => "required"
  ];

  public static function boot() {
    parent::boot();
    static::creating(function($model) {
              \Cache::forget('ServicesCache');
            });
    static::updating(function($model) {
              \Cache::forget('ServicesCache');
            });
    static::deleted(function($model) {
              \Cache::forget('ServicesCache');
            });
  }

  public function getDates() {
    return ['created_at', 'updated_at'];
  }

  public static function rules($id) {
    $rules = static::$rules;

    return $rules;
  }

  public static function getServices() {
    return \Cache::remember('ServicesCache', 60, function() {
                      return self::first();
                    });
  }

}
