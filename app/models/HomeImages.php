<?php

class HomeImages extends BaseModel {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'home_images';
  protected $fillable = array('image', 'type');
  static $rules = [
      'image' => "required",
      'type' => "required"
  ];
  protected $appends = ['file'];

  public static function boot() {
    parent::boot();
    static::creating(function($model) {
              \Cache::forget('HomeImagesCache');
              \Cache::forget('HomeImagesWebSiteCache');
              \Cache::forget('HomeImagesAppCache');
            });
    static::updating(function($model) {
              \Cache::forget('HomeImagesCache');
              \Cache::forget('HomeImagesWebSiteCache');
              \Cache::forget('HomeImagesAppCache');
            });
    static::deleted(function($model) {
              \Cache::forget('HomeImagesCache');
              \Cache::forget('HomeImagesWebSiteCache');
              \Cache::forget('HomeImagesAppCache');
            });
  }

  public function getDates() {
    return ['created_at', 'updated_at'];
  }

  public function getFileAttribute() {
    return url("assets/images/site/home/" . $this->attributes['image']);
  }

  public static function allImages() {
    return \Cache::remember('HomeImagesCache', 60, function() {
                      return self::all();
                    });
  }

  public static function allWebSiteImages() {
    return \Cache::remember('HomeImagesWebSiteCache', 60, function() {
                      return self::where('type', 'web')
                                      ->get()
                                      ->toArray();
                    });
  }

  public static function allAppImages() {
    return \Cache::remember('HomeImagesAppCache', 60, function() {
                      return self::where('type', 'mobile')
                                      ->get()
                                      ->toArray();
                    });
  }

}
