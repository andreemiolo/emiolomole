<?php

namespace helpers;

class Helpers {

    public static function getCep($cep) {
        $arrA = \Correios::cep(urlencode($cep));
        if (count($arrA) > 0) {
            $logradouro = explode("-", $arrA['logradouro']);
            return array(
                'street' => trim($logradouro[0]),
                'neighborhood' => trim($arrA['bairro']),
                'city' => trim($arrA['cidade']),
                'state' => trim($arrA['uf'])
            );
        }

        return array();
    }

    public static function setReplaceDecimal($valor) {
        $valor = str_replace(".", "", $valor);
        $valor = str_replace(",", ".", $valor);
        return $valor;
    }

    public static function getReplaceDecimal($valor) {
        return number_format($valor, 2, ',', '.');
    }

    public static function uploadImage($path, $file, $crop = false, $coordinates = []) {
        if (!\File::isDirectory(public_path($path))) {
            \File::makeDirectory(public_path($path), 777, true);
        }

        $intervation = \Image::make($file->getRealPath());
        if ($crop) {
            if (count($coordinates) == 4) {
                $x = $coordinates['x'];
                $y = $coordinates['y'];
                $w = $coordinates['w'];
                $h = $coordinates['h'];
                //crop(int $width, int $height, [int $x, int $y])
                $intervation->crop($w, $h, $x, $y);
            }
        }
        $getClientOriginalName = md5(date("dmYHis") . $file->getClientOriginalName());
        $getClientOriginalExtension = $file->getClientOriginalExtension();
        $name_file = "$getClientOriginalName.$getClientOriginalExtension";
        $intervation->save(public_path("$path$name_file"), 100);

        return $name_file;
    }

}
